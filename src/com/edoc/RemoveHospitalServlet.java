package com.edoc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RemoveHospitalServlet
 */
public class RemoveHospitalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveHospitalServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
try {
			
			// Registering Driver
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String remvhid=request.getParameter("rem_hospital_id");
		try {
		    
		     Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe","system","oracle");
			 PreparedStatement ps=con.prepareStatement("delete from hospital where hospid=?");
			 ps.setString(1, remvhid);
			 
			 int some=ps.executeUpdate();
			 if(some!=0){
				 
				response.sendRedirect("home4.html"); 
				 
				 
			 }
			 con.close();
			 
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		
	}

}

package com.edoc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PatientRegServlet
 */
public class PatientRegServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PatientRegServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
try {
			
			// Registering Driver
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String uid_p=request.getParameter("userid_p");
		String fname_p=request.getParameter("firstname_p");
		String lname_p=request.getParameter("lastname_p");
		String symp=request.getParameter("symptoms");
		String pass_p=request.getParameter("password_p");
		String dp=request.getParameter("dob_p");
		String gen_p=request.getParameter("sex_p");
		String add_p=request.getParameter("message_p");
		String num_p=request.getParameter("phone_no_p");
		String email_p=request.getParameter("email_p");
		
		try {
		    
		     Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe","system","oracle");
			 PreparedStatement ps=con.prepareStatement("insert into patients values(?,?,?,?,?,?,?,?,?,?)");
			 ps.setString(1, uid_p);
			 ps.setString(2, fname_p);
			 ps.setString(3, lname_p);
			 ps.setString(4, symp);
			 ps.setString(5, pass_p);
			 ps.setString(6, dp);
			 ps.setString(7, gen_p);
			 ps.setString(8, add_p);
			 ps.setString(9, num_p);
			 ps.setString(10, email_p);
			 int some=ps.executeUpdate();
			 if(some!=0){
				 
				response.sendRedirect("login_patient.html"); 
				 
				 
			 }
			 con.close();
			 
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

}

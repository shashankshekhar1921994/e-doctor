package com.edoc;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SearchHospServlet
 */
public class SearchHospServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchHospServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
try {
			
			// Registering Driver
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out4=response.getWriter();
		String city_h =request.getParameter("search_hospital");
		
		try {
			
			
		Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe","system","oracle");
		PreparedStatement ps=con.prepareStatement("select name,address,phone from hospital where city=?");
		ps.setString(1, city_h);
		ResultSet rs=ps.executeQuery();
	    while(rs.next()){
	    	String a=rs.getString("name");
	    	String b=rs.getString("address");
	    	String c=rs.getString("phone");
	    	out4.println("Name-"+a+" Address-"+b+" Phone-"+c);
	    	
	    }
	     
		con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

package com.edoc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddHospitalServlet
 */
public class AddHospitalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddHospitalServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
try {
			
			// Registering Driver
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String hospid=request.getParameter("hospital_id");
		String hname=request.getParameter("hospital_name");
		String haddress=request.getParameter("hospital_address");
		String hphone=request.getParameter("hospital_number");
		String hcity=request.getParameter("hospital_city");
		
		try {
		    
		     Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/xe","system","oracle");
			 PreparedStatement ps=con.prepareStatement("insert into hospital values(?,?,?,?,?)");
			 ps.setString(1, hospid);
			 ps.setString(2, hname);
			 ps.setString(3, haddress);
			 ps.setString(4, hphone);
			 ps.setString(5, hcity);
			
			 int some=ps.executeUpdate();
			 if(some!=0){
				 
				response.sendRedirect("home3.html"); 
				 
				 
			 }
			 con.close();
			 
			 
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
	}

}
